'use strict'

const nconf = require('nconf')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

let debugSql = nconf.get('DEBUG_SQL')

const sequelize = new Sequelize('sqlite://:memory:', {
  operatorsAliases: Op,
  logging: debugSql ? console.log : false
})

module.exports = sequelize
