'use strict'

const express = require('express')
const userController = require('./api/user/controller')
const friendController = require('./api/friend/controller')

// error handling middleware
const wrap = fn => (...args) => fn(...args).catch(args[2])

module.exports = () => {
  let api = express.Router()

  api.get('/users', wrap(userController.list))
  api.post('/users', wrap(userController.insert))
  api.put('/users/:userId', wrap(userController.update))
  api.get('/users/:userId', wrap(userController.get))

  api.get('/friends/:userId', wrap(friendController.get))
  api.post('/friends/connect/:userId/:friendId', wrap(friendController.connect))
  api.post('/friends/accept/:userId/:friendId', wrap(friendController.accept))

  // error handling if function wrap catches something wrong
  api.use(function (err, req, res, next) {
    if (err) {
      // console.log(err)
      if (err.errors) return res.status(500).send({'errors': err.errors.map(e => e.message)})
      return res.status(500).send(err)
    }
  })

  return api
}
