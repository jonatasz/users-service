'use strict'

const Sequelize = require('sequelize')
const sequelize = require('./../../db/orm')

// model definition
const Friend = sequelize.define('friend', {
  accepted: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
    allowNull: false
  }
})

module.exports = Friend
