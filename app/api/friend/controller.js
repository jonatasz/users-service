'use strict'

const User = require('./../user/model')
const Friend = require('./model')

/**
* This function returns an user
* params: id (integer)
**/
async function get (req, res, next) {
  let id = req.params.userId
  let user = await User.findById(id)
  if (!user) res.status(500).send({'error': 'No user found'})
  let friends = await user.getUserFriends()
  return res.json(friends)
};

/**
* This function returns an user
* params: id (integer)
**/
async function connect (req, res, next) {
  let userId = req.params.userId
  let friendId = req.params.friendId

  let user = await User.findById(userId)
  let friend = await User.findById(friendId)

  if (!user) return res.status(500).send({'error': 'No user found'})
  if (!friend) return res.status(500).send({'error': 'No user friend found'})

  await Friend.create({
    userId: userId,
    userFriendId: friendId
  })

  let friendship = await Friend.findOne({where: {
    userId: userId,
    userFriendId: friendId
  }})
  return res.json(friendship)
};

/**
* This function returns an user
* params: id (integer)
**/
async function accept (req, res, next) {
  let userId = req.params.userId
  let friendId = req.params.friendId

  let friendship = await Friend.findOne({where: {
    userId: userId,
    userFriendId: friendId,
    accepted: false
  }})

  if (!friendship) return res.status(500).send({'error': 'No friendship found'})

  await friendship.updateAttributes({
    accepted: true
  })

  return res.json(friendship)
};

module.exports = {
  'get': get,
  'connect': connect,
  'accept': accept
}
