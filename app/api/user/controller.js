'use strict'

const User = require('./model')
const Op = require('sequelize').Op

/**
* This function returns an user
* params: id (integer)
**/
async function get (req, res, next) {
  let id = req.params.userId
  let user = await User.findById(id)

  res.json(user)
};

/**
* This function returns a list of users
**/
async function list (req, res, next) {
  let users = await User.findAll()

  res.json(users)
};

/**
* This function will create a User on the database
*
**/
async function insert (req, res) {
  let user = req.body
  return res.json(await User.create(user))
};

/**
* This function will create a User on the database
*
**/
async function update (req, res) {
  let user = req.body
  let userId = req.params.userId
  if (!user || !userId) return res.status(500).send({'error': 'Please send an user with id'})

  let updated = await User.update(user, {
    where: {
      id: {
        [Op.eq]: userId
      }
    }
  })

  return res.json(updated)
};

module.exports = {
  'get': get,
  'list': list,
  'insert': insert,
  'update': update
}
