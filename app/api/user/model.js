'use strict'

const Sequelize = require('sequelize')
const sequelize = require('./../../db/orm')

const Friend = require('./../friend/model')

// model definition
const User = sequelize.define('user', {
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      isEmail: true
    }
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: [4]
    }
  }
},
  {
    indexes: [
      {
        fields: ['name']
      },
      {
        unique: true,
        fields: ['email']
      }
    ]
  }
)

User.belongsToMany(User, {as: 'userFriends', through: Friend})

module.exports = User
