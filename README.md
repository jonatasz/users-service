# How to run

**Installation**

Node version: v8.9.1

Run these commands inside project folder:

1. npm install
2. npm install -g mocha


------------


**How to test**

1. You can import a file to POSTMAN and see the examples
2. You can run the mocha tests (npm test)


------------


** Tech chosen **

Express.js, because Yoti uses it. Today I use Koa at the startup I work for

SQLite, just to run the database in memory, no need to install any database for this tech test. If I were to choose for a project, I would use PostgreSQL for it, or even MySQL.


------------


** Whats is missing **

Login, Admin user, searching by name and email (not supported in SQLite), API documentation (Swagger)


------------


** ENDPOINTS Available **

1. GET: /api/users/:userId
	* Returns an user
2. GET: /api/users
	* List available users
3. POST: /api/users
	* Insert an user
	* body: {name: string, email: string, password: string}
4. PUST: /api/users/:userId
	* Update an user
	* body: {name: string, email: string, password: string}



------------------


1. GET: /api/friends/:userId
	* Returns all friends of an user
2. POST: /api/friends/connect/:userId/:friendId
	* Makes a connection between an User and a possible Friend
3. POST: /api/friends/accept/:userId/:friendId
	* Accepts a connection between an User and a Friend
