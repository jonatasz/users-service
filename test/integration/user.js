'use strict'

const agent = require('superagent')
const db = require('./../../app/db/orm')

// chai lib
const chai = require('chai')
const assert = chai.assert

const appUrl = 'http://localhost:3000/api'

describe('user controller', () => {
  describe('insert user', () => {
    describe('with empty object', () => {
      it('should throw an error', async function () {
        try {
          await agent.post(appUrl + '/users').send({})
        } catch (err) {
          assert.equal(500, err.status)
        }
      })
    })

    describe('with invalid name and password', () => {
      it('should throw an error', async function () {
        try {
          await agent.post(appUrl + '/users').send({email: 'user1@gmail.com'})
        } catch (err) {
          assert.equal(500, err.status)
          assert.isArray(err.response.body.errors)
          assert.equal('user.name cannot be null', err.response.body.errors[0])
          assert.equal('user.password cannot be null', err.response.body.errors[1])
        }
      })
    })

    describe('with invalid email', () => {
      it('should throw an error', async function () {
        try {
          await agent.post(appUrl + '/users').send({name: 'User 1', password: '1234', email: 'email'})
        } catch (err) {
          assert.equal(500, err.status)
          assert.isArray(err.response.body.errors)
          assert.equal('Validation isEmail on email failed', err.response.body.errors[0])
        }
      })
    })

    describe('with valid user information', () => {
      it('should return', async function () {
        let response = await agent.post(appUrl + '/users').send({name: 'User 1', password: '1234', email: 'email@gmail.com'})
        assert.equal(200, response.statusCode)
        assert.equal(1, response.body.id)
        assert.equal('User 1', response.body.name)
        assert.equal('1234', response.body.password)
        assert.equal('email@gmail.com', response.body.email)
      })

      after(async function () {
        let response = await agent.get(appUrl + '/users/1')
        assert.equal('User 1', response.body.name)
      })
    })
  })

  describe('update user', () => {
    before(async function () {
      await db.sync({ force: true }) // drop and create table
      await agent.post(appUrl + '/users').send({name: 'User 1', password: '1234', email: 'email@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 2', password: '1234', email: 'email2@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 3', password: '1234', email: 'email3@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 4', password: '1234', email: 'email4@gmail.com'})
    })

    describe('with empty object', () => {
      it('should throw an error', async function () {
        try {
          await agent.put(appUrl + '/users/1').send({})
        } catch (err) {
          assert.equal(500, err.status)
          assert.equal('Please send an user with id', err.response.body.error)
        }
      })
    })

    describe('with invalid name and password', () => {
      it('should throw an error', async function () {
        try {
          await agent.put(appUrl + '/users/1').send({email: 'user1@gmail.com'})
        } catch (err) {
          assert.equal(500, err.status)
          assert.isArray(err.response.body.errors)
          assert.equal('user.name cannot be null', err.response.body.errors[0])
          assert.equal('user.password cannot be null', err.response.body.errors[1])
        }
      })
    })

    describe('with invalid email', () => {
      it('should throw an error', async function () {
        try {
          await agent.put(appUrl + '/users/1').send({name: 'User 1', password: '1234', email: 'email'})
        } catch (err) {
          assert.equal(500, err.status)
          assert.isArray(err.response.body.errors)
          assert.equal('Validation isEmail on email failed', err.response.body.errors[0])
        }
      })
    })

    describe('with valid user information', () => {
      it('should return', async function () {
        let response = await agent.put(appUrl + '/users/1').send({name: 'User 1 updated', password: '1234', email: 'email@gmail.com'})
        assert.equal(200, response.statusCode)
        assert.isArray(response.body)
        assert.equal(1, response.body[0])
      })

      after(async function () {
        let response = await agent.get(appUrl + '/users/1')
        assert.equal('User 1 updated', response.body.name)
      })
    })
  })

  describe('list users', () => {
    before(async function () {
      await db.sync({ force: true }) // drop and create table
      await agent.post(appUrl + '/users').send({name: 'User 1', password: '1234', email: 'email@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 2', password: '1234', email: 'email2@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 3', password: '1234', email: 'email3@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 4', password: '1234', email: 'email4@gmail.com'})
    })
    it('should list', async function () {
      let response = await agent.get(appUrl + '/users')
      assert.equal(200, response.statusCode)
      assert.isArray(response.body)
      assert.equal(4, response.body.length)
      assert.equal('email@gmail.com', response.body[0].email)
      assert.equal('email4@gmail.com', response.body[3].email)
    })
  })
})

describe('friend controller', () => {
  describe('connect users', () => {
    before(async function () {
      await db.sync({ force: true }) // drop and create table
      await agent.post(appUrl + '/users').send({name: 'User 1', password: '1234', email: 'email@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 2', password: '1234', email: 'email2@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 3', password: '1234', email: 'email3@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 4', password: '1234', email: 'email4@gmail.com'})
    })

    describe('with invalid users', () => {
      it('should throw an error', async function () {
        try {
          await agent.post(appUrl + '/friends/connect/1/10')
        } catch (err) {
          assert.equal(500, err.status)
          assert.equal('No user friend found', err.response.body.error)
        }
      })
    })

    describe('with valid users', () => {
      it('should connect', async function () {
        let response = await agent.post(appUrl + '/friends/connect/1/2')
        assert.equal(false, response.body.accepted)
        assert.equal(1, response.body.userId)
        assert.equal(2, response.body.userFriendId)
      })

      after(async function () {
        let response = await agent.post(appUrl + '/friends/connect/1/3')
        assert.equal(false, response.body.accepted)
        assert.equal(1, response.body.userId)
        assert.equal(3, response.body.userFriendId)
      })
    })
  })

  describe('accept friendship', () => {
    before(async function () {
      await db.sync({ force: true }) // drop and create table
      await agent.post(appUrl + '/users').send({name: 'User 1', password: '1234', email: 'email@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 2', password: '1234', email: 'email2@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 3', password: '1234', email: 'email3@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 4', password: '1234', email: 'email4@gmail.com'})
    })

    // connect users
    before(async function () {
      let response = await agent.post(appUrl + '/friends/connect/1/2')
      assert.equal(false, response.body.accepted)
      assert.equal(1, response.body.userId)
      assert.equal(2, response.body.userFriendId)
    })

    before(async function () {
      let response = await agent.post(appUrl + '/friends/connect/1/3')
      assert.equal(false, response.body.accepted)
      assert.equal(1, response.body.userId)
      assert.equal(3, response.body.userFriendId)
    })

    describe('with invalid users', () => {
      it('should throw an error', async function () {
        try {
          await agent.post(appUrl + '/friends/accept/1/10')
        } catch (err) {
          assert.equal(500, err.status)
          assert.equal('No friendship found', err.response.body.error)
        }
      })
    })

    describe('with valid users', () => {
      it('should return friendship', async function () {
        let response = await agent.post(appUrl + '/friends/accept/1/2')
        assert.equal(true, response.body.accepted)
      })
    })
  })

  describe('list friendships', () => {
    before(async function () {
      await db.sync({ force: true }) // drop and create table
      await agent.post(appUrl + '/users').send({name: 'User 1', password: '1234', email: 'email@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 2', password: '1234', email: 'email2@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 3', password: '1234', email: 'email3@gmail.com'})
      await agent.post(appUrl + '/users').send({name: 'User 4', password: '1234', email: 'email4@gmail.com'})
    })

    // connect users
    before(async function () {
      let response = await agent.post(appUrl + '/friends/connect/1/2')
      assert.equal(false, response.body.accepted)
      assert.equal(1, response.body.userId)
      assert.equal(2, response.body.userFriendId)
    })

    before(async function () {
      let response = await agent.post(appUrl + '/friends/connect/1/3')
      assert.equal(false, response.body.accepted)
      assert.equal(1, response.body.userId)
      assert.equal(3, response.body.userFriendId)
    })

    before(async function () {
      let response = await agent.post(appUrl + '/friends/accept/1/2')
      assert.equal(true, response.body.accepted)
    })

    it('should list friendship', async function () {
      let response = await agent.get(appUrl + '/friends/1')
      assert.equal('email2@gmail.com', response.body[0].email)
      assert.equal(true, response.body[0].friend.accepted)
      assert.equal('email3@gmail.com', response.body[1].email)
      assert.equal(false, response.body[1].friend.accepted)
    })
  })
})
