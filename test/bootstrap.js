'use strict'

var server

// stop server after all tests runned
before(async function () {
  server = await require('../index')
})

// stop server after all tests runned
after(async function () {
  await server.close()
  console.log('Server stopped!')
})
