'use strict'

// load env variables (app/config.json)
const nconf = require('nconf')
nconf.use('file', {file: './app/config.json'})

const express = require('express')
const app = express()
const routes = require('./app/routes')

const bodyParser = require('body-parser')

const db = require('./app/db/orm')

db.sync({ force: true })

app.use(bodyParser.json())
app.use('/api', routes())

module.exports = app.listen(3000, () => console.log('Users service listening on port 3000!\n'))
